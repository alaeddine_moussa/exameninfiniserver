package entities;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Commentaire
 *
 */
@Entity
public class Commentaire implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCommentaire;
	private String text_commentaire;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Bugs bugs;
	
	

	public Commentaire() {
		super();
	}

	public int getIdCommentaire() {
		return this.idCommentaire;
	}

	public void setIdCommentaire(int idCommentaire) {
		this.idCommentaire = idCommentaire;
	}

	public String getText_commentaire() {
		return this.text_commentaire;
	}

	public void setText_commentaire(String text_commentaire) {
		this.text_commentaire = text_commentaire;
	}

	public Bugs getBugs() {
		return bugs;
	}

	public void setBugs(Bugs bugs) {
		this.bugs = bugs;
	}

}
