package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Bugs
 *
 */
@Entity
public class Bugs implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBugs;
	private int numero_bug;
	private String priorite;
	private int bug_liaison;
	private String description;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Users users;
	@OneToMany(mappedBy = "bugs", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Commentaire> commentaires;

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Bugs() {
		super();
	}

	public Bugs(int numero_bug, String priorite, int bug_liaison,
			String description) {
		super();
		this.numero_bug = numero_bug;
		this.priorite = priorite;
		this.bug_liaison = bug_liaison;
		this.description = description;
	}

	public void affectCommToBugs(List<Commentaire> listComm) {
		for (Commentaire commentaire : listComm) {
			commentaire.setBugs(this);
		}
		this.commentaires = listComm;
	}

	public int getIdBugs() {
		return this.idBugs;
	}

	public void setIdBugs(int idBugs) {
		this.idBugs = idBugs;
	}

	public int getNumero_bug() {
		return this.numero_bug;
	}

	public void setNumero_bug(int numero_bug) {
		this.numero_bug = numero_bug;
	}

	public String getPriorite() {
		return this.priorite;
	}

	public void setPriorite(String priorite) {
		this.priorite = priorite;
	}

	public int getBug_liaison() {
		return this.bug_liaison;
	}

	public void setBug_liaison(int bug_liaison) {
		this.bug_liaison = bug_liaison;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

}
