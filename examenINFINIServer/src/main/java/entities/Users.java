package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Users
 *
 */
@Entity
public class Users implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUsers;
	private String nom;
	private String prenom;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<Bugs> bugs;

	public Users() {
		super();
	}

	public void affectUserToBugs(List<Bugs> listBugs) {
		for (Bugs bugs : listBugs) {
			bugs.setUsers(this);
		}
		this.bugs = listBugs;
	}

	public int getIdUsers() {
		return this.idUsers;
	}

	public void setIdUsers(int idUsers) {
		this.idUsers = idUsers;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Bugs> getBugs() {
		return bugs;
	}

	public void setBugs(List<Bugs> bugs) {
		this.bugs = bugs;
	}

}
