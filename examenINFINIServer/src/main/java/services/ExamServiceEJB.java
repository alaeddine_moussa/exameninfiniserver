package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.Bugs;
import entities.Commentaire;
import entities.Users;

/**
 * Session Bean implementation class ExamServiceEJB
 */
@Stateless
public class ExamServiceEJB implements ExamServiceEJBRemote {
	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public ExamServiceEJB() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean addUsers(Users users) {
		try {
			em.persist(users);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean addCommentToBugs(int numero_bug,
			List<Commentaire> commentaires) {
		try {
			Bugs bugs = em
					.createQuery("select b from Bugs b where b.numero_bug=?1",
							Bugs.class).setParameter(1, numero_bug)
					.getSingleResult();
			bugs.setCommentaires(commentaires);
			bugs.affectCommToBugs(commentaires);
			em.merge(bugs);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public List<Bugs> getHistoryBugs() {
		return em.createQuery("select b from Bugs b", Bugs.class)
				.getResultList();
	}

}
