package services;

import java.util.List;

import javax.ejb.Remote;

import entities.Bugs;
import entities.Commentaire;
import entities.Users;

@Remote
public interface ExamServiceEJBRemote {
	/**
	 * this class add a users
	 * @param users
	 * @return
	 */
	public boolean addUsers(Users users);
	public boolean addCommentToBugs(int numero_bug,List<Commentaire> commentaires);
	public List<Bugs> getHistoryBugs();
}
